# Jenkins | Testing functionality

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## 1. Context
1. Install and configure the Slack plugin to integrate notifications.
2. You will need to create a dedicated channel for these notifications.
3. Install the plugin: [https:plugins.jenkins.io/embeddable-build-status/](https:plugins.jenkins.io/embeddable-build-status/) and add the list to the README.md file of the repository after creating the pipeline.
4. Modify the Jenkinsfile to include the task of sending success or failure notifications of the pipeline via Slack.

## 2. Requirements
- Set up a CI pipeline on Jenkins for this lab: [Check out this procedure](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab5-deploiement)
- [Use this code : The Alpinehelloworld code + Jenkinsfile + Badge on GitHub](https://github.com/CarlinFongang/alpinehelloworld)

## 3. Slack Configuration
1. Create a Slack account and workspace.
2. Create a Slack channel named "jenkins-notification".
   >![alt text](img/image.png)
3. Install the Jenkins plugin on Slack.
   Go to Add apps > "Search Jenkins", then Add.
   >![alt text](img/image-1.png)
   >![alt text](img/image-2.png)
   >![alt text](img/image-3.png)

4. Install the Slack plugin on Jenkins.
   Dashboard > Manage Jenkins > "Search: Slack Notification"
   >![alt text](img/image-4.png)
   *Slack plugin on Jenkins*

5. Connect Slack to Jenkins CI:
   - Create a text type secret for Slack authentication.
     Dashboard > Manage Jenkins > Credentials > Add Domain (global) > Add Credentials
     >![alt text](img/image-5.png)

   - Retrieve the secret from Slack.
     >![alt text](img/image-6.png)

   - Fill in the information retrieved from Slack on Jenkins and validate.
     >![alt text](img/image-7.png)

   - Configure the Slack plugin from Jenkins.
     Dashboard > Administer Jenkins > System > "Slack Section"
     Fill in the information provided by Slack and test the connection.
     >![alt text](img/image-8.png)
     *Slack connection successful*

## 4. Update the Jenkinsfile to integrate notifications
````
        post {
            success {
                slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
            failure {
                slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
        }
````
### Adding Slack Notification Code after Pipeline Execution
>![alt text](img/image-9.png)
*Pipeline Execution*

>![alt text](img/image-10.png)
*Successful Slack Notification Transfer*

## 5. Adding Badges
1. To install the plugin that will be used to add badges:
   Dashboard > Manage Jenkins > Plugin > Available Plugins
   >![alt text](img/image-11.png)
   *Badge Installation*

2. Access the project to configure the badge:
Dashboard > alpinehelloworld > Embeddable Build Status > Markdown
Choose Markdown unprotected
`[![Build Status](http://44.222.244.69:8080/buildStatus/icon?job=alpinehelloworld)](http://44.222.244.69:8080/job/alpinehelloworld/)`
>![alt text](img/image-12.png)

3. Add the code to the README.md of the source project on GitHub and commit the changes
>![alt text](img/image-13.png)

4. Result
>![alt text](img/image-14.png)
